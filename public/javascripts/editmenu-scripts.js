/**
 * Created with IntelliJ IDEA.
 * User: Artyom
 * Date: 05.06.13
 * Time: 2:53
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {
    $("body").on('click', '.addDishButton', function () {
        document.getElementById("success-message").setAttribute("hidden", "true");
        document.getElementById("error-message").setAttribute("hidden", "true");
        var id_s = $(this).attr("sort_id");
        var newDish = {
            "name": $(document.getElementById("addDish").getElementsByClassName("inputName")[0]).val(),
            "weight": $(document.getElementById("addDish").getElementsByClassName("inputWeight")[0]).val(),
            "price": $(document.getElementById("addDish").getElementsByClassName("inputPrice")[0]).val(),
            "ingredients": $(document.getElementById("addDish").getElementsByClassName("inputIngredients")[0]).val(),
            "description": $(document.getElementById("addDish").getElementsByClassName("textareaDescription")[0]).val()
        }
        var js = JSON.stringify(newDish);
        jsRoutes.controllers.AdministrationController.addDish(id_s).ajax({
            type: "POST",
            data: js,
            dataType: 'text',   //тип ответа
            contentType: 'json',
            success: function (data) {
                alert("Успех");
                $('#addForm').setAttribute("action", @routes.AdministrationController.upload(data));
                $('#addForm').submit();
                document.getElementById("success-message").innerText = data;
                document.getElementById("success-message").setAttribute("hidden", "false");
            },
            error: function (data) {
                alert("Неудача");
                document.getElementById("error-message").innerText = data;
                document.getElementById("error-message").setAttribute("hidden", "false");
            }
        });
    });
});

$(document).ready(function () {
    $("body").on('click', '.acceptDishButton', function () {
        alert("!");
        document.getElementById("success-message").setAttribute("hidden", "true");
        document.getElementById("error-message").setAttribute("hidden", "true");
        var id = $(this).attr("dish_id");
        var id_s = $(this).attr("sort_id");
        //var src = $(document.getElementById(id).getElementsByClassName("dish-image")[0]).attr("src").val();
        //alert(src);
        var newDish = {
            "name": $(document.getElementById(id).getElementsByClassName("inputName")[0]).val(),
            "weight": $(document.getElementById(id).getElementsByClassName("inputWeight")[0]).val(),
            "price": $(document.getElementById(id).getElementsByClassName("inputPrice")[0]).val(),
            "ingredients": $(document.getElementById(id).getElementsByClassName("inputIngredients")[0]).val(),
            "description": $(document.getElementById(id).getElementsByClassName("textareaDescription")[0]).val()
        }
        var js = JSON.stringify(newDish);
        jsRoutes.controllers.AdministrationController.changeDish(id_s, id).ajax({
            type: "PUT",
            data: js,
            dataType: 'text',   //тип ответа
            contentType: 'application/json',
            success: function (data) {
                alert("Успех");
                $('#form'+id).submit();
                document.getElementById("success-message").innerText = data;
                document.getElementById("success-message").setAttribute("hidden", "false");
            },
            error: function (data) {
                alert("Неудача");
                document.getElementById("error-message").innerText = data;
                document.getElementById("error-message").setAttribute("hidden", "false");
            }
        });
    });
});
