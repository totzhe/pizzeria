/**
 * Created with IntelliJ IDEA.
 * User: Artyom
 * Date: 01.06.13
 * Time: 1:24
 * To change this template use File | Settings | File Templates.
 */

//setTimeout(updateOrders, 1000);

$(document).ready(function () {
    $("body").on('click', '.btn-send', function () {
        var id = $(this).attr("id");
        jsRoutes.controllers.AdministrationController.sendOrder(id).ajax({
            type: "POST",
            data: null,
            dataType: 'text',   //тип ответа
            success: function (data) {
                //alert(data + "#order" + id);
                $("#order" + id).remove();
            },
            error: function () {
                alert(data);
            }
        });
    });
});

function updateOrders() {
    jsRoutes.controllers.AdministrationController.getNewOrders().ajax({
        type: "GET",
        data: null,
        dataType: 'text',   //тип ответа
        success: function (data) {
            //alert(data);
            data = $.parseJSON(data);
            $.each(data, function () {
                //alert(this.id + " ");
                if ($("#order" + this.id).length == 0) {
                    var date = new Date(this.recievingTime);

                    var i = 0;
                    var items = '';
                    $.each(this.items, function () {
                        //alert(this.id + " i");
                        i++;
                        items = items + '\
                        <tr>\
                            <th><center>' + i + '</center></th>\
                            <th>' + this.dish.name + '</th>\
                            <th><center>' + this.quantity + '</center></th>\
                        </tr>'
                    });

                    $("#order-container").append('\
                        <div class="thumbnail group-in-container" id="order' + this.id + '">\
                        <div class="row" id="info">\
                        <div class="span2">\
                        <strong>Заказ</strong> <nobr id="order-number">' + this.id + '</nobr>\
                        </div>\
                    <div class="span4" id="order-date">\
                     <strong>Время приема</strong> <nobr id="order-time">' + ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2) + ":" + ("0" + date.getSeconds()).slice(-2) + '</nobr>    \
                     </div>    \
                     </div>      \
                     <div class="row span10" id="table-form" xmlns="http://www.w3.org/1999/html">       \
                     <table class="table table-striped table-bordered table-condensed" id="table-order"> \
                     <thead>        \
                     <tr>           \
                     <th width="40"><center>#</center></th>   \
                     <th>Блюдо</th>  \
                     <th width="40">Кол-во</th>  \
                     </tr>      \
                     </thead>  \
                     <tbody>   \
                    ' + items + '\
                    </tbody>\
                        </table> \
                    </div>   \
                        <div class="row"> \
                            <div class="span1 offset8"> \
                                <button class="btn btn-success btn-send" id="' + this.id + '">Отправить</button>\
                            </div> \
                        </div> \
                    </div> \
                    ');
                }
                else {
                    //alert(this.id);
                }
            });
        },
        error: function () {
            alert("Error!");
        }
    });
    //$("#order-container").append('1');
    setTimeout(updateOrders, 1000);
}

$(document).ready(function () {
    //$("body").on('click', '#btn', function () {
    updateOrders();
    //});
});