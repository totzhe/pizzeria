package controllers;

import daos.AdminDao;
import daos.DishSortDao;
import daos.GenericDao;
import flexjson.JSONSerializer;
import models.Dish;
import models.DishSort;
import models.Order;
import org.codehaus.jackson.JsonNode;
import play.Play;
import play.db.jpa.Transactional;
import play.mvc.*;
import services.OrderManagementService;
import services.ShowMenuService;
import views.html.indexadmin;
import views.html.menu;
import views.html.orders;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Alina
 * Date: 29.05.13
 * Time: 23:13
 * To change this template use File | Settings | File Templates.
 */
@Security.Authenticated(Secured.class)
public class AdministrationController extends Controller {

    @Transactional
    public static Result indexadmin(int id) {
        String st = "";
        return ok(indexadmin.render(id, new ShowMenuService().getDishSorts(),
                new ShowMenuService().getDishSortById(id).getDishes()));
    }

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss:SS");

    @Transactional
    public static Result getOrders() {
        List<Order> activeOrders = new OrderManagementService().getActiveOrders();
        if (!activeOrders.isEmpty()) {
            session().put("lastUpdate", dateFormat.format(activeOrders.get(activeOrders.size() - 1).getRecievingTime()));
        }
        return ok(orders.render(activeOrders));
    }

    @Transactional
    public static Result getNewOrders() {
        String date = session("lastUpdate");
        Date lastUpdate = new Date(0);
        if (date != null) {
            try {
                lastUpdate = dateFormat.parse(date);
            } catch (ParseException e) {
                lastUpdate = new Date(0);
            }
        }
        Date currentUpdate = new Date();
        List<Order> orders = new OrderManagementService().getNewOrders(lastUpdate, currentUpdate);
        if (!orders.isEmpty()) {
            session().put("lastUpdate", dateFormat.format(orders.get(orders.size() - 1).getRecievingTime()));
        }
        JSONSerializer serializer = new JSONSerializer().include("items").exclude("class", "items.class", "items.orderId", "items.dish.class", "items.dish.sort", "items.dish.picturePath", "items.dish.description");
        return ok(serializer.serialize(orders));
    }

    @Transactional
    public static Result sendOrder(int id) {
        if (new OrderManagementService().sendOrder(id))
            return redirect(routes.Application.ordersent());//ok("Order was sent.");
        else
            return badRequest("Order" + id + " does not exist.");
    }

    @BodyParser.Of(play.mvc.BodyParser.Json.class)
    @Transactional
    public static Result changeDish(int id, int dish_id) {
        response().setContentType("application/json");
        GenericDao<Dish, Integer> dao = new GenericDao<Dish, Integer>(Dish.class);
        Dish dish = dao.read(dish_id);

        JsonNode json = request().body().asJson();

        if (json == null) {
            return badRequest("Expecting Json data");
        } else {
            String name = json.findPath("name").getTextValue();
            Integer weight = Integer.parseInt(json.findPath("weight").getTextValue().trim());
            Integer price = Integer.parseInt(json.findPath("price").getTextValue().trim());
            String ingredients = json.findPath("ingredients").getTextValue();
            String description = json.findPath("description").getTextValue();
            if (name == null) {
                return badRequest("Упущен параметр [name]");
            }

            if (weight == null) {
                return badRequest("Упущен параметр [weight]");
            }

            if (price == null) {
                return badRequest("Упущен параметр [price]");
            }

            if (ingredients == null) {
                return badRequest("Упущен параметр [ingredients]");
            }

            if (description == null) {
                return badRequest("Упущен параметр [description]");
            }

            dish.setName(name);
            dish.setWeight(weight);
            dish.setPrice(price);
            dish.setIngredients(ingredients);
            dish.setDescription(description);

            dao.update(dish);

            return ok("Блюдо " + name + " успешно обновлено");
        }
    }

    @Transactional
    public static Result upload(int id) {
        //File fl = play.api.Play.application().getFile();
        //fl.getPath();
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart picture = body.getFile("picture");
        if (picture != null) {
            String fileName = picture.getFilename();
            String contentType = picture.getContentType();
            File file = picture.getFile();
            String myUploadPath = Play.application().configuration().getString("myUploadPath");
            fileName = UUID.randomUUID().toString() + "-" + fileName;
            file.renameTo(new File(myUploadPath, fileName));
            //
            GenericDao<Dish, Integer> dao = new GenericDao<Dish, Integer>(Dish.class);
            Dish dish = dao.read(id);
            if (dish == null)
                return badRequest("Dish does not exist.");
            dish.setPicturePath(Play.application().configuration().getString("AssetsDishImgPath") + fileName);
            dao.update(dish);
            return redirect(routes.AdministrationController.indexadmin(dish.getSort().getId()));
        } else {
            flash("error", "Missing file");
            return redirect(routes.AdministrationController.indexadmin(id));
        }
    }

    @Transactional
    public static Result addDish(int id) {
        response().setContentType("application/json");
        GenericDao<Dish, Integer> dao = new GenericDao<Dish, Integer>(Dish.class);

        JsonNode json = request().body().asJson();

        if (json == null) {
            return badRequest("Expecting Json data");
        } else {
            String name = json.findPath("name").getTextValue();
            Integer weight = Integer.parseInt(json.findPath("weight").getTextValue().trim());
            Integer price = Integer.parseInt(json.findPath("price").getTextValue().trim());
            String ingredients = json.findPath("ingredients").getTextValue();
            String description = json.findPath("description").getTextValue();
            if (name == null) {
                return badRequest("Упущен параметр [name]");
            }

            if (weight == null) {
                return badRequest("Упущен параметр [weight]");
            }

            if (price == null) {
                return badRequest("Упущен параметр [price]");
            }

            if (ingredients == null) {
                return badRequest("Упущен параметр [ingredients]");
            }

            if (description == null) {
                return badRequest("Упущен параметр [description]");
            }

            Dish dish = new Dish();
            DishSortDao sortDao = new DishSortDao();

            dish.setSort(sortDao.read(id));
            dish.setName(name);
            dish.setWeight(weight);
            dish.setPrice(price);
            dish.setIngredients(ingredients);
            dish.setDescription(description);

            dao.create(dish);

            return ok(((Integer)dish.getId()).toString());
        }
    }

}
